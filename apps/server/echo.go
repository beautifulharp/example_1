//
// RIT Tech Platform - Sample Componnet (1)
//
//
package main

import (
	"fmt"

	"github.com/golang/glog"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	pb "../pb"
)

// Implements of EchoServiceServer

type echoServer struct{}

func newEchoServer() pb.EchoServiceServer {
	return new(echoServer)
}

// Method-1:
func (s *echoServer) Echo(ctx context.Context, msg *pb.SimpleMessage) (*pb.SimpleMessage, error) {
	glog.Info(msg)

	fmt.Print("OK:echo_1, ")
	fmt.Print(msg)
	fmt.Print("\n")
	return msg, nil
}

// Method-2:
func (s *echoServer) EchoBody(ctx context.Context, msg *pb.SimpleMessage) (*pb.SimpleMessage, error) {
	glog.Info(msg)

	fmt.Print("OK:echo_2, ")
	fmt.Print(msg)
	fmt.Print("\n")

	grpc.SendHeader(ctx, metadata.New(map[string]string{
		"foo": "foo1",
		"bar": "bar1",
	}))
	grpc.SetTrailer(ctx, metadata.New(map[string]string{
		"foo": "foo2",
		"bar": "bar2",
	}))
	return msg, nil
}
