package main

import (
	"flag"
	"net"

	"github.com/golang/glog"
	"google.golang.org/grpc"
	pb "../pb"
)

func Run() error {
	// @TODO: To set up the port-number from the configuration
	l, err := net.Listen("tcp", ":9090")
	if err != nil {
		return err
	}
	s := grpc.NewServer()
	pb.RegisterEchoServiceServer(s, newEchoServer())
	s.Serve(l)
	return nil
}

func main() {
	flag.Parse()
	defer glog.Flush()

	if err := Run(); err != nil {
		glog.Fatal(err)
	}
}
